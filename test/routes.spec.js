// routes.spec.js
// some imports etc...
import { app } from '../src/main.js';
import routes from '../src/routes.js';
import { describe, it } from 'mocha';
import { expect } from 'chai';
/** @type {import('node:http').Server} */
let server;
const API_URL = 'http://localhost:3000/api/v1';
describe("REST API Routes", () => {
    before(() => {
        app.use('/api/v1', routes);
        server = app.listen(3001, () => console.log('Test server started...'));
    });
    it("can get response", async () => {
        const response = await fetch(`${API_URL}/`);
        expect(response.ok).to.be.true; // fast check
    });
    it("should convert RGB to HEX correctly", async () => {
        const response = await fetch(`${API_URL}/rgb-to-hex?red=255&green=136&blue=0`);
        const text = await response.text();
        expect(text).to.equal("#ff8800");
    });
    it("should convert HEX to RGB correctly", async () => {
        const response = await fetch(`${API_URL}/hex-to-rgb?hex=ff8800`);
        const text = await response.text();
        expect(text).to.equal('{"red":255,"green":136,"blue":0}');
    });
    it("should convert HEX to RGB correctly 2nd", async () => {
        const response = await fetch(`${API_URL}/hex-to-rgb?hex=c8859b`);
        const text = await response.text();
        expect(text).to.equal('{"red":200,"green":133,"blue":155}');
    });
    after((done) => {
        server.close(() => done());
    });
});