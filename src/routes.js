// src/routes.js
import { Router } from "express";
import * as conv from './converter.js';
const routes = Router();

// Endpoint GET '{{api_url}}/'
routes.get('/', (req, res) => res.status(200).send("Welcome!"));

// Endpoint GET '{{api_url}}/rgb-to-hex?red=255&green=133&blue=0'
// http://localhost:3000/api/v1/rgb-to-hex?red=200&green=133&blue=155
routes.get('/rgb-to-hex', (req, res) => {
// collect query parameters "red", "green" and "blue"
// sanitize and/or convert data
// convert RGB values to HEX value
// respond with HEX value
    const red = parseInt(req.query.red, 10);
    const green = parseInt(req.query.green, 10);
    const blue = parseInt(req.query.blue, 10);
    res.status(200).send(conv.rgb_to_hex(red, green, blue));

});

// Endpoint GET '{{api_url}}/hex-to-rgb?hex=c8859b'
// http://localhost:3000/api/v1/hex-to-rgb?hex=c8859b
routes.get('/hex-to-rgb', (req, res) => {
    if (!req.query.hex) {
        return res.status(400).json({ error: 'Missing hex query parameter' });
    }
    const hex = req.query.hex.toString();  
    res.status(200).json(conv.hex_to_rgb(hex));
});
export default routes;