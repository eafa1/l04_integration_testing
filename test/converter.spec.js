import { describe, it } from 'mocha';
import { expect } from 'chai';
import { rgb_to_hex, hex_to_rgb } from '../src/converter.js';
/**
* converter.js tests
* @command npm run test
* 
*/
describe("RGB-to-HEX Converter", () => {
    it("is a function", () => {
        expect(rgb_to_hex).to.be.an('function');
    });
    it("should return a string", () => {
        expect(rgb_to_hex(0, 0, 0)).to.be.a('string');
    });
    it("first character is hastag", () => {
        expect(rgb_to_hex(0, 0, 0)[0]).to.equal("#");
    });
    it("should convert RED value correctly", () => {
        expect(rgb_to_hex( 0, 0, 0).substring(0, 3)).to.equal("#00");
        expect(rgb_to_hex(255, 0, 0).substring(0, 3)).to.equal("#ff");
        expect(rgb_to_hex(136, 0, 0).substring(0, 3)).to.equal("#88");
        expect(rgb_to_hex(136, 0, 0).substring(0, 3)).to.not.equal("#00");
    });
    it("should convert GREEN value correctly", () => {
        expect(rgb_to_hex( 0, 0, 0).substring(0, 3)).to.equal("#00");
        expect(rgb_to_hex( 0,255, 0).substring(3, 5)).to.equal("ff");
        expect(rgb_to_hex( 0,136, 0).substring(3, 5)).to.equal("88");
        expect(rgb_to_hex( 0,136, 0).substring(3, 5)).to.not.equal("00");
    });
    it("should convert BLUE value correctly", () => {
        expect(rgb_to_hex( 0, 0, 0).substring(0, 3)).to.equal("#00");
        expect(rgb_to_hex( 0, 0,255).substring(5, 7)).to.equal("ff");
        expect(rgb_to_hex( 0, 0,136).substring(5, 7)).to.equal("88");
        expect(rgb_to_hex( 0, 0,136).substring(5, 7)).to.not.equal("00");
    });
    it("should convert RGB to HEX correctly", () => {
        expect(rgb_to_hex( 0, 0, 0)).to.equal("#000000");
        expect(rgb_to_hex(255,255,255)).to.equal("#ffffff");
        expect(rgb_to_hex(255,136, 0)).to.equal("#ff8800");
        expect(rgb_to_hex(255,136, 0)).to.not.equal("#00ff00");
        // expect(rgb_to_hex(10000000,136, 0)).to.not.equal("#00ff00");
    });
});

describe("HEX-to-RGB Converter", () => {
    it("is a function", () => {
        expect(hex_to_rgb).to.be.an('function');
    });
    it("should return a json object", () => {
        expect(hex_to_rgb("#000000")).to.be.an('object');
    });
    it("response json should contain correct key labels", () => {
        expect(hex_to_rgb("#000000")).to.have.all.keys('red', 'green', 'blue');
    });
    it("should convert RED value# correctly", () => {
        expect(hex_to_rgb('00')).to.deep.equal({red:0,green:NaN,blue:NaN});
        expect(hex_to_rgb('ff')).to.deep.equal({red:255,green:NaN,blue:NaN});
        expect(hex_to_rgb("88")).to.deep.equal({red:136,green:NaN,blue:NaN});
        expect(hex_to_rgb("00")).to.not.deep.equal({red:136,green:NaN,blue:NaN});
    });
    it("should convert GREEN value correctly", () => {
        expect(hex_to_rgb('0000')).to.deep.equal({red:0,green:0,blue:NaN});
        expect(hex_to_rgb('00ff')).to.deep.equal({red:0,green:255,blue:NaN});
        expect(hex_to_rgb("0088")).to.deep.equal({red:0,green:136,blue:NaN});
        expect(hex_to_rgb("0000")).to.not.deep.equal({red:0,green:136,blue:NaN});
    });
    it("should convert BLUE value correctly", () => {

        expect(hex_to_rgb('000000')).to.deep.equal({red:0,green:0,blue:0});
        expect(hex_to_rgb('0000ff')).to.deep.equal({red:0,green:0,blue:255});
        expect(hex_to_rgb("000088")).to.deep.equal({red:0,green:0,blue:136});
        expect(hex_to_rgb("000000")).to.not.deep.equal({red:0,green:0,blue:136});
    });
    it("should convert HEX to RGB correctly", () => {
        /* expect(hex_to_rgb("000000")).to.equal({"red":0,"green":0,"blue":0});
        expect(hex_to_rgb("#ffffff")).to.equal('{"red":255,"green":255,"blue":255}');
        expect(hex_to_rgb("#ff8800")).to.equal('{"red":255,"green":136,"blue":0}');
        expect(hex_to_rgb("#00ff00")).to.not.equal('{"red":255,"green":136,"blue":0}'); */
        expect(hex_to_rgb("000000")).to.deep.equal({red:0,green:0,blue:0});
        expect(hex_to_rgb("ffffff")).to.deep.equal({red:255,green:255,blue:255});
        expect(hex_to_rgb("ff8800")).to.deep.equal({red:255,green:136,blue:0});
        expect(hex_to_rgb("00ff00")).to.not.deep.equal({red:255,green:136,blue:0});
        // expect(hex_to_rgb(10000000,136, 0)).to.not.equal("#00ff00");
    });
});