// src/converter.js

/**
* Padding hex component if necessary.
* Hex component representation requires
* two hexadecimal characters.
* @param {string} comp
* @returns {string} two hexadecimal characters
*/
const pad = (comp) => {
    const padded = comp.length == 2 ? comp : "0" + comp;
    return padded;
}

/**
* RGB to HEX conversion
* @param {number} r RED
* @param {number} g GREEN
* @param {number} b BLUE
* @returns {string} in hex color format, e.g., "#00ff00" (green)
*/
export const rgb_to_hex = (r, g, b) => {
    let hex = "#";
    // conversion here
    // consider fixed width (amount of characters)...
    // fill hex
    const hexR = r.toString(16);
    const hexG = g.toString(16);
    const hexB = b.toString(16);
    return hex + pad(hexR) + pad(hexG) + pad(hexB);
};

/**
* HEX to RGB  conversion
* @param {string} hex in hex color format, e.g., "#00ff00" (green)
* @returns {number, number, number} in RGB number format e.g., "255,255,0"
*/
export const hex_to_rgb = (hex) => {
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4), 16);
    return { red: r, green: g, blue: b };
};

