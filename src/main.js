// src/main.js
// node ./src/main.js
import express from 'express';
import routes from './routes.js';
export const app = express();
const PORT = 3000;
app.use(`/api/v1`, routes);
app.listen(PORT, () => {});
